//
//  VideosListViewCell.swift
//  VIPER_Architechture_Task
//
//  Created by ArrowTN on 11/09/2017.
//  Copyright © 2017 ArrowTN. All rights reserved.
//

import UIKit
import AlamofireImage

class VideoCollectionCell: UICollectionViewCell{
    
    @IBOutlet weak var epsLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var videoImageView: UIImageView!
    
    func set(forVideos video: VideoModel){
        nameLabel?.text = video.name
        epsLabel?.text = video.eps
        let url = URL(string: video.imgUrl)!
        let image_from_url_request: URLRequest = URLRequest(url: url)
        
        NSURLConnection.sendAsynchronousRequest(image_from_url_request, queue: OperationQueue.main, completionHandler: {(response: URLResponse!, data: Data!, error: Error!) -> Void in
        
            if error == nil && data != nil {
                self.videoImageView.image = UIImage(data: data)
            }
        })
    }
}
